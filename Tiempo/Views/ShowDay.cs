﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Content;
using System.Collections.Generic;
using Android.Graphics.Drawables;
using System.IO;
using Tiempo.Presenters;

namespace Tiempo.Views
{
    [Activity(Label = "Tiempo")]
    public class ShowDay : Activity,IShowDayRecycler
    {
        private IPresenter2 presenter;
        string city;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.show_day);
            city = Intent.GetStringExtra("Locality");
            presenter = new Presenter2(this,BaseContext);
            presenter.FindCity(city);
            FindViewById<Button>(Resource.Id.Buttonshow).Click += OnClick;
        }     
        
                
        void OnItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            presenter.OnItemClic(city, e.Position);

        }
        

        void OnClick(object sender, EventArgs e)
        {
            presenter.OnClick(city);
        }
       

        public void ShowOneDay(DayWeather dayweather, List<Hour> weatherlist)
        {
            var image = FindViewById<ImageView>(Resource.Id.image);
            var temp = FindViewById<TextView>(Resource.Id.temperature);
            var city3 = FindViewById<TextView>(Resource.Id.city);
            var description = FindViewById<TextView>(Resource.Id.description);
            var minmax = FindViewById<TextView>(Resource.Id.minmax);
            temp.Text = dayweather.main.temp.ToString();
            city3.Text = city + "," + dayweather.sys.country;
            description.Text = dayweather.weather[0].description;
            minmax.Text = dayweather.main.temp_min.ToString() + "º/" + dayweather.main.temp_max.ToString()+"º";
            Stream stream = Assets.Open("images/" + dayweather.weather[0].icon + ".png");
            Drawable drawable = Drawable.CreateFromStream(stream, null);
            image.SetImageDrawable(drawable);
            var weatherList = FindViewById<ListView>(Resource.Id.showmore);
            weatherList.Adapter = new WeatherAdapter(weatherlist);
            weatherList.ItemClick += OnItemClick;
        }
    }
}