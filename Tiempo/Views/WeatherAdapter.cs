﻿using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using Android.Graphics.Drawables;
using System.IO;

namespace Tiempo
{
    public class WeatherAdapter:BaseAdapter<Hour>
    {
        List<Hour> WeatherHours;
        public WeatherAdapter(List<Hour> weather)
        {
            this.WeatherHours = weather;
        }
        public override Hour this[int position] 
        { 
            get 
            {
                return WeatherHours[position];
            }
        }
        public override int Count
        {
            get
            {
                return WeatherHours.Count;
            }
        }
        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var inflater = LayoutInflater.From(parent.Context);
            var view = inflater.Inflate(Resource.Layout.weather_row,parent, false);

            var image = view.FindViewById<ImageView>(Resource.Id.photoView);
            Stream stream = parent.Context.Assets.Open("images/" + WeatherHours[position].weather[0].icon + ".png");
            Drawable drawable = Drawable.CreateFromStream(stream, null);
            image.SetImageDrawable(drawable);

            var fecha = view.FindViewById<TextView>(Resource.Id.fecha);
            fecha.Text = WeatherHours[position].dt_txt;

            var description = view.FindViewById<TextView>(Resource.Id.description);
            description.Text = WeatherHours[position].weather[0].description;

            var temperature = view.FindViewById<TextView>(Resource.Id.temperature);
            temperature.Text = WeatherHours[position].main.temp.ToString()+"ºC";


            var minmax = view.FindViewById<TextView>(Resource.Id.minmax);
            minmax.Text = WeatherHours[position].main.temp_min.ToString()+ "º/" + WeatherHours[position].main.temp_max.ToString()+"º";
            return view;
        }

    }
}