﻿using Android.Views;
using Android.Widget;
using System;
using Android.Support.V7.Widget;

namespace Tiempo
{
    public class WeatherViewHolder : RecyclerView.ViewHolder
    {
        Action<int> listener;
        public ImageView Image { get; private set; }
        public TextView Date { get; private set; }
        public TextView Description { get; private set; }
        public TextView Temp { get; private set; }
        public TextView Minmax { get; private set; }
        


        public WeatherViewHolder(View itemView,Action<int> listener) : base(itemView)
        {
            Image = itemView.FindViewById<ImageView>(Resource.Id.photoView);
            Date = itemView.FindViewById<TextView>(Resource.Id.date);
            Description = itemView.FindViewById<TextView>(Resource.Id.description);
            Temp = itemView.FindViewById<TextView>(Resource.Id.temperature);
            Minmax = itemView.FindViewById<TextView>(Resource.Id.minmax);
          
            this.listener = listener;
            itemView.Click += OnClick;
        }
        void OnClick(object sender, EventArgs e)
        {
            int position = AdapterPosition;
            if (position == RecyclerView.NoPosition)
                return;
            listener(position);
        }
        
    }
}