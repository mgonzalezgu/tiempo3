﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tiempo.Views
{
    public interface IShowMoreDay
    {
        void TakeWeather(DayWeather day, string direction, string v1, string v2);
    }
}