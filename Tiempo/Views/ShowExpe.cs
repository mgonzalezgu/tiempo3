﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Graphics.Drawables;
using System.IO;
using Tiempo.Presenters;
using Tiempo.Views;

namespace Tiempo
{
    [Activity(Label = "Tiempo")]
    public class ShowExpe : Activity,IShowExpe
    {
        string city;
        int position;
        private IPresenter4 presenter;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.show_expe);
            city = Intent.GetStringExtra("Locality");
            position = Intent.GetIntExtra("Position", -1);
            presenter = new Presenter4(this);
            presenter.FindCity(city,position);
        }

        public void ShowDayWeather(Hour hour, string direc)
        {
            var date = FindViewById<TextView>(Resource.Id.date);
           
            date.Text = hour.dt_txt;
            

            var image = FindViewById<ImageView>(Resource.Id.image);
            Stream stream = Assets.Open("images/" + hour.weather[0].icon + ".png");
            Drawable drawable = Drawable.CreateFromStream(stream, null);
            image.SetImageDrawable(drawable);

            var temp = FindViewById<TextView>(Resource.Id.temperature);
            temp.Text = hour.main.temp + "ºC";

            var city3 = FindViewById<TextView>(Resource.Id.city);
            city3.Text = city;

            var description = FindViewById<TextView>(Resource.Id.description);
            description.Text =hour.weather[0].description;

            var minmax = FindViewById<TextView>(Resource.Id.minmax);
            minmax.Text = hour.main.temp_min.ToString() + "º/" + hour.main.temp_max.ToString()+"º";

            var confort = FindViewById<ImageView>(Resource.Id.conforticon);
            Stream stream1 = Assets.Open("images/confort_level.png");
            Drawable drawable1 = Drawable.CreateFromStream(stream1, null);
            confort.SetImageDrawable(drawable1);

            var humidity = FindViewById<TextView>(Resource.Id.humidity);
            humidity.Text = hour.main.humidity + "%";

            var feelslike = FindViewById<TextView>(Resource.Id.feelslike);
            feelslike.Text = "Feels like: " + hour.main.feels_like.ToString() + "ºC";

            var presure = FindViewById<TextView>(Resource.Id.pressure);
            presure.Text = "Pressure: " + hour.main.pressure.ToString() + "hPa";

            var clouds = FindViewById<TextView>(Resource.Id.clouds);
            clouds.Text = "Clouds: " + hour.clouds.all.ToString() + "%";

            var wind = FindViewById<ImageView>(Resource.Id.windicon);
            Stream stream2 = Assets.Open("images/wind.png");
            Drawable drawable2 = Drawable.CreateFromStream(stream2, null);
            wind.SetImageDrawable(drawable2);

            var speed = FindViewById<TextView>(Resource.Id.speed);
            speed.Text = "Speed: " + hour.wind.speed.ToString() + "m/s";

            var direction = FindViewById<TextView>(Resource.Id.direction);
            direction.Text = direc;
        }
    }
}