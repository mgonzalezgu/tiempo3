﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Content;
using System.Collections.Generic;
using Android.Graphics.Drawables;
using System.IO;
using Android.Support.V7.Widget;
using Tiempo.Views;
using Tiempo.Presenters;

namespace Tiempo
{
    [Activity(Label = "Tiempo")]
    
    public class ShowDayRecycler : Activity, IShowDayRecycler
    {
        RecyclerView rv;
        string city;
        private IPresenter2 presenter;
        
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.con_recycler);
            city = Intent.GetStringExtra("Locality");
            presenter = new Presenter2(this,BaseContext);
            presenter.FindCity(city);
            FindViewById<Button>(Resource.Id.Buttonshow).Click += OnClick;

        }
    
        public void ShowOneDay(DayWeather day, List<Hour> weatherlist)
        {
            var image = FindViewById<ImageView>(Resource.Id.image);
            Console.WriteLine(day);
            Stream stream = Assets.Open("images/" + day.weather[0].icon + ".png");
            Drawable drawable = Drawable.CreateFromStream(stream, null);
            image.SetImageDrawable(drawable);

            var temp = FindViewById<TextView>(Resource.Id.temperature);
            temp.Text = day.main.temp.ToString()+"ºC";

            var city = FindViewById<TextView>(Resource.Id.city);
            city.Text = day.name + "," + day.sys.country;

            var description = FindViewById<TextView>(Resource.Id.description);
            description.Text = day.weather[0].description;

            var minmax = FindViewById<TextView>(Resource.Id.minmax);
            minmax.Text = day.main.temp_min.ToString() + "º/" + day.main.temp_max.ToString()+"º";
            

            rv = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            rv.SetLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.Vertical, false));
            var adapter = new RecyclerAdapter(weatherlist);
            adapter.ItemClick += OnItemClick;
            rv.SetAdapter(adapter);
        }
        void OnItemClick(object sender, int position)
        {
            presenter.OnItemClic(city, position);
        }
        void OnClick(object sender, EventArgs e)
        {
            presenter.OnClick(city);
        }
    }
}