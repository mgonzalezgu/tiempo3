﻿using System;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using Android.Content;
using Tiempo.Presenters;


namespace Tiempo.Views
{
    [Activity(Label = "Tiempo", Icon = "@drawable/weathericon", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity,IMainActivity
    {
        EditText city;
        private IPresenters1 presenter;
        protected override void OnCreate(Bundle bundle)
        {

            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_main);
            city = FindViewById<EditText>(Resource.Id.TextCity);
            presenter = new Presenters1(this,BaseContext);
            FindViewById<Button>(Resource.Id.FindButton).Click += OnClick;
        }

        public void OnClick(object sender, EventArgs e)
        {
            Toast.MakeText(this, "Loading", ToastLength.Short).Show();
            presenter.OnClick(city.Text);
        }

        

        public void InvalidName()
        {
            Toast.MakeText(this, "Write a valid city name", ToastLength.Short).Show();
            
        }

        public void StartNewActivity()
        {
            var intent = new Intent(this, typeof(ShowDayRecycler));
            intent.PutExtra("Locality", city.Text);
            StartActivity(intent);
        }
    }
}
