﻿using System;
using Android.Views;
using Android.Content;
using System.Collections.Generic;
using Android.Graphics.Drawables;
using System.IO;
using Android.Support.V7.Widget;

namespace Tiempo
{
    public class RecyclerAdapter: RecyclerView.Adapter
    {
        List<Hour> mydata;
        Context context;
        public RecyclerAdapter(List<Hour> data)
        {
            this.mydata = data;
        }
        public override int ItemCount
        {
            get { return mydata.Count; }
        }
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            this.context = parent.Context;
            var inflater = LayoutInflater.From(parent.Context);
            var view = inflater.Inflate(Resource.Layout.recycler_weather,parent,false);
            return new WeatherViewHolder(view,OnClick);
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            
            var vh = (WeatherViewHolder)holder;
            Stream stream = context.Assets.Open("images/" + mydata[position].weather[0].icon + ".png");
            Drawable drawable = Drawable.CreateFromStream(stream, null);
            vh.Image.SetImageDrawable(drawable);

            vh.Date.Text = mydata[position].dt_txt;
            vh.Temp.Text = mydata[position].main.temp.ToString()+"ºC";

            vh.Minmax.Text = mydata[position].main.temp_min.ToString()+"º/"+ mydata[position].main.temp_max.ToString()+"º";

            vh.Description.Text = mydata[position].weather[0].description;

        }
        public event EventHandler<int> ItemClick;
        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }
    }
    
}