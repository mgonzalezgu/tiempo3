﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tiempo.Views
{
    public class NavigatorImpl : INavigator
    {
        Context context;
        public NavigatorImpl(Context context)
        {
            this.context = context;
        }
        public void NextPage(string city)
        {

            var intent = new Intent(context, typeof(ShowDayRecycler));
            //var intent = new Intent(context, typeof(ShowDay));
            intent.PutExtra("Locality", city);

            context.StartActivity(intent.AddFlags(ActivityFlags.NewTask));
        }

        public void ShowExpe(string city, int position)
        {
            var intent = new Intent(context, typeof(ShowExpe));
            intent.PutExtra("Position", position);
            intent.PutExtra("Locality", city);
            context.StartActivity(intent.AddFlags(ActivityFlags.NewTask));
        }

        public void ShowMore(string city)
        {

            var intent = new Intent(context, typeof(ShowMoreDay));
            intent.PutExtra("Locality", city);
            context.StartActivity(intent.AddFlags(ActivityFlags.NewTask));
        }
    }
}