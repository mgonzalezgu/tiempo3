﻿using Android.App;
using Android.OS;
using Android.Widget;
using Android.Content;
using Android.Graphics.Drawables;
using System.IO;
using Tiempo.Presenters;
using Tiempo.Views;

namespace Tiempo
{
    [Activity(Label = "Tiempo")]
    public class ShowMoreDay : Activity,IShowMoreDay
    {
        private IPresenter3 presenter;
        string city;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.show_more);
            city = Intent.GetStringExtra("Locality");
            presenter = new Presenter3(this);
            presenter.FindCity(city);
        }
        
        public void TakeWeather(DayWeather day, string direction, string v1, string v2)
        {
            var image = FindViewById<ImageView>(Resource.Id.image);
            Stream stream = Assets.Open("images/" + day.weather[0].icon + ".png");
            Drawable drawable = Drawable.CreateFromStream(stream, null);
            image.SetImageDrawable(drawable);

            var temp = FindViewById<TextView>(Resource.Id.temperature);
            temp.Text = day.main.temp.ToString() + "ºC";

            var city3 = FindViewById<TextView>(Resource.Id.city);
            city3.Text = city + "," + day.sys.country;

            var description = FindViewById<TextView>(Resource.Id.description);
            description.Text = day.weather[0].description;

            var minmax = FindViewById<TextView>(Resource.Id.minmax);
            minmax.Text = day.main.temp_min.ToString() + "º/" + day.main.temp_max.ToString()+"º";

            var confort = FindViewById<ImageView>(Resource.Id.conforticon);
            Stream stream1 = Assets.Open("images/confort_level.png");
            Drawable drawable1 = Drawable.CreateFromStream(stream1, null);
            confort.SetImageDrawable(drawable1);

            var humidity = FindViewById<TextView>(Resource.Id.humidity);
            humidity.Text = day.main.humidity.ToString() + "%";

            var feelslike = FindViewById<TextView>(Resource.Id.feelslike);
            feelslike.Text = "Feels like: " + day.main.feels_like.ToString() + "ºC";

            var presure = FindViewById<TextView>(Resource.Id.pressure);
            presure.Text = "Pressure: " + day.main.pressure.ToString() + "hPa";

            var clouds = FindViewById<TextView>(Resource.Id.clouds);
            clouds.Text = "Clouds: " + day.clouds.all.ToString() + "%";

            var wind = FindViewById<ImageView>(Resource.Id.windicon);
            Stream stream2 = Assets.Open("images/wind.png");
            Drawable drawable2 = Drawable.CreateFromStream(stream2, null);
            wind.SetImageDrawable(drawable2);

            var speed = FindViewById<TextView>(Resource.Id.speed);
            speed.Text = "Speed: " + day.wind.speed.ToString() + "m/s";

            var direc = FindViewById<TextView>(Resource.Id.direction);
            direc.Text = direction;

            var sunrise = FindViewById<TextView>(Resource.Id.sunrise);
            sunrise.Text = v1;

            var sunriseicon = FindViewById<ImageView>(Resource.Id.sunriseicon);
            Stream stream3 = Assets.Open("images/sunrise.jpg");
            Drawable drawable3 = Drawable.CreateFromStream(stream3, null);
            sunriseicon.SetImageDrawable(drawable3);

            var sunseticon = FindViewById<ImageView>(Resource.Id.sunseticon);
            Stream stream4 = Assets.Open("images/sunset.png");
            Drawable drawable4 = Drawable.CreateFromStream(stream4, null);
            sunseticon.SetImageDrawable(drawable4);

            var sunset = FindViewById<TextView>(Resource.Id.sunset);
            sunset.Text = v2;
        }
    }
}

