﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Globalization;

namespace Tiempo
{
    public class DayWeather
    {
        
        public List<Weather> weather { get; set; }
        public Main main { get; set; }
        public Wind wind { get; set; }
        public Percent clouds { get; set; }
        public Sys sys { get; set; }
        public int timezone { get; set; }
        public string name { get; set; }

    }
    public class Weather
    {
        public string description { get; set; }
        public string icon { get; set; }
        
    }
    public class Main
    {
        public float temp { get; set; }
        public float feels_like { get; set; }
        public float temp_min { get; set; }
        public float temp_max { get; set; }
        public int pressure { get; set; }
        public int humidity { get; set; }
    }
    public class Wind
    {
        public float speed { get; set; }
        public float deg { get; set; }

        public string Direction(float deg)
        {
            if (deg < 22.5 && deg > 337.5)
            {
                return "North, " + deg.ToString() + "º";
            }
            else if (deg >= 22.5 && deg <= 67.5)
            {
                return "Northeast, " + deg.ToString() + "º";
            }
            else if (deg > 67.5 && deg < 112.5)
            {
                return "East, " + deg.ToString() + "º";
            }
            else if (deg >= 112.5 && deg <= 157.5)
            {
                return "Southeast, " + deg.ToString() + "º";
            }
            else if (deg > 157.5 && deg < 202.5)
            {
                return "South, " + deg.ToString() + "º";
            }
            else if (deg >= 202.5 && deg <= 247.5)
            {
                return "Southwest, " + deg.ToString() + "º";
            }
            else if (deg > 247.5 && deg < 292.5)
            {
                return "West, " + deg.ToString() + "º";
            }
            else
            {
                return "Northwest, " + deg.ToString() + "º";
            }
        }
    }
    public class Percent
    {
        public int all { get; set; }
    }
    public class Sys
    {
        public string country { get; set; }
        public int sunrise { get; set; }
        public int sunset { get; set; }

    }
    public class HoursWeather
    {
        public List<Hour> List { get; set; }
        public string name { get; set; }
    }

    public class Hour
    {
        public Main main { get; set; }
        public List<Weather> weather { get; set; }
        public Percent clouds { get; set; }
        public Wind wind { get; set; }
        public string dt_txt { get; set; }

        public string ToDate(string date)
        {
            DateTime data = DateTime.Parse(date, CultureInfo.InvariantCulture);

            var dt = data.ToString("f", CultureInfo.CreateSpecificCulture("en-US"));
            string[] date1 = dt.Split(" ");

            return date1[0].Substring(0, 3) + ", " + date1[1].Substring(0, 3) + " " + date1[2] + date1[4].Split(":")[0] + " " + date1[5];
        }

    }
}