﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tiempo.Presenters;
using Tiempo.Views;
using Tiempo.Models;

namespace Tiempo.Presenters
{
    public class Presenters1 : IPresenters1
    {
        private IMainActivity view;
        private Interactor1 interactor;
        INavigator navigator;
        Urls url;
        string city;

        public Presenters1(IMainActivity view,Context context)
        {

            this.view = view;
            interactor = new Interactor1(this);
            navigator = new NavigatorImpl(context);
            url = new Urls();
        }
       
        void IPresenters1.InvalidName()
        {
            view.InvalidName();
        }

        public void OnClick(string city)
        {
            this.city = city;
            if (string.IsNullOrEmpty(city))
            {
                view.InvalidName();
            }
            else 
            {
                interactor.FindWeather(url.CatchUrl(1,city));
            }

        }

        public void TakeCity()
        {
            navigator.NextPage(city);
        }
    }
}