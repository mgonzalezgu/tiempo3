﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tiempo.Views;
using Tiempo.Models;
using System.Globalization;

namespace Tiempo.Presenters
{
    public class Presenter2:IPresenter2
    {
        private IShowDayRecycler view;
        private IInteractor2 interactor;
        INavigator navigator;
        Urls url;

        public Presenter2(IShowDayRecycler view,Context context)
        {
            this.view = view;
            interactor = new Interactor2(this);
            url = new Urls();
            navigator = new NavigatorImpl(context);
        }

        public void FindCity(string city)
        {
            var url1 = url.CatchUrl(1,city);
            var url2 = url.CatchUrl(2,city);
            interactor.TakeWeather(url1,url2);
        }

        public void OnClick(string city)
        {
            navigator.ShowMore(city);
        }

        public void OnItemClic(string city, int position)
        {
            navigator.ShowExpe(city,position);
        }

        public void TakeWeather(DayWeather dayweather, HoursWeather hoursweather)
        {
            var hourList = hoursweather.List;
            foreach (Hour hour in hourList)
            {
                hour.dt_txt = hour.ToDate(hour.dt_txt);
                hour.weather[0].description = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(hour.weather[0].description);
                hour.main.temp = (float)Math.Round(hour.main.temp);
                hour.main.temp_max = (float)Math.Round(hour.main.temp_max);
                hour.main.temp_min = (float)Math.Round(hour.main.temp_min);
            }
            dayweather.weather[0].description = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(dayweather.weather[0].description);
            dayweather.main.temp = (float)Math.Round(dayweather.main.temp);
            dayweather.main.temp_max = (float)Math.Round(dayweather.main.temp_max);
            dayweather.main.temp_min = (float)Math.Round(dayweather.main.temp_min);
            view.ShowOneDay(dayweather, hourList);
        }
    }
}