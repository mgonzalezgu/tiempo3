﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tiempo.Presenters
{
    public interface IPresenter2
    {
        void FindCity(string city);
        void TakeWeather(DayWeather dayweather, HoursWeather hoursweather);
        void OnClick(string city);
        void OnItemClic(string city, int position);
    }
}