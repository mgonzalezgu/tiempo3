﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tiempo.Presenters
{
    public class Urls
    {
        public string url;
        public string apiBase1;
        public string apiBase2;
        public string units;

        public Urls()
        {
            apiBase1 = "http://api.openweathermap.org/data/2.5/weather?q=";
            apiBase2 = "http://api.openweathermap.org/data/2.5/forecast?q=";
            units = "metric";
            url = "&appid=828971f619b9ba9b1632c23dff01bec5&units="+ units;
        }

        public string CatchUrl(int number,string city)
        {
            if (number == 1)
            {
                return apiBase1 + city + url;
            }
            else
            {
                return apiBase2 + city + url;
            }
        }
        
        
    }
}