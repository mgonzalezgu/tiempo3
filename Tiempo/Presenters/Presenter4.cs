﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tiempo.Views;
using Tiempo.Models;
using System.Globalization;

namespace Tiempo.Presenters
{
    public class Presenter4: IPresenter4
    {
        private IShowExpe view;
        private Interactor4 interactor;
        Urls url;
        int pos;

        public Presenter4(IShowExpe view)
        {
            this.view = view;
            interactor = new Interactor4(this);
            url = new Urls();
        }

        public void FindCity(string city, int position)
        {
            var url1 = url.CatchUrl(2, city);
            pos = position;
            interactor.TakeWeather(url1);
        }

        public void TakeWeather(HoursWeather hour)
        {
            var onehour = hour.List[pos];
            var direction = onehour.wind.Direction(onehour.wind.deg);
            onehour.dt_txt = onehour.ToDate(onehour.dt_txt);
            onehour.weather[0].description = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(onehour.weather[0].description);
            onehour.main.temp = (float)Math.Round(onehour.main.temp);
            onehour.main.temp_max = (float)Math.Round(onehour.main.temp_max);
            onehour.main.temp_min = (float)Math.Round(onehour.main.temp_min);
            onehour.main.feels_like = (float)Math.Round(onehour.main.feels_like);
            view.ShowDayWeather(onehour,direction);
        }
    }
}