﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tiempo.Views;
using Tiempo.Models;
using System.Globalization;

namespace Tiempo.Presenters
{
    public class Presenter3 : IPresenter3
    {
        private IShowMoreDay view;
        private Interactor3 interactor;
        Urls url;

        public Presenter3(IShowMoreDay view)
        {
            this.view = view;
            interactor = new Interactor3(this);
            url = new Urls();
        }
        public void FindCity(string city)
        {
            string url1 = url.CatchUrl(1, city);
            
            interactor.TakeWeather(url1);
        }

        public void TakeWeather(DayWeather day)
        {
            var direction = day.wind.Direction(day.wind.deg);
            day.weather[0].description = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(day.weather[0].description);
            day.main.temp = (float)Math.Round(day.main.temp);
            day.main.temp_max = (float)Math.Round(day.main.temp_max);
            day.main.temp_min = (float)Math.Round(day.main.temp_min);
            day.main.feels_like = (float)Math.Round(day.main.feels_like);
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            var ss = dateTime.AddSeconds(day.sys.sunrise + day.timezone).ToString() + " " + dateTime.AddSeconds(day.sys.sunset + day.timezone).ToString();
            string[] separar = ss.Split(' ');
            string[] sinseg1 = separar[1].Split(":");
            string[] sinseg2 = separar[3].Split(":");
            var rise = sinseg1[0] + ":" + sinseg1[1];
            var set = sinseg2[0] + ":" + sinseg2[1];
            view.TakeWeather(day, direction, rise, set);
        }
    }
}