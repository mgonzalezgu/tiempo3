﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tiempo.Presenters;

namespace Tiempo.Models
{
    public class Interactor3: IInteractor3
    {
        private IPresenter3 presenter;
        TakeData data;
        public Interactor3(Presenter3 presenter)
        {
            this.presenter = presenter;
            data = new TakeData();
        }
        async public void TakeWeather(string url1)
        {
            await data.TakeDay(url1);
            var day = data.dayweather;
            presenter.TakeWeather(day);
        }
    }
}