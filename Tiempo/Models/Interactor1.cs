﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Android.Content;
using System.Collections.Generic;
using System.Net;
using Android.Graphics.Drawables;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;
using Tiempo.Presenters;

namespace Tiempo.Models
{
    public class Interactor1:IInteractor1
    {
        private IPresenters1 presenter;
        TakeData data;
        
        public Interactor1(IPresenters1 presenter)
        {
            this.presenter = presenter;
            data = new TakeData();
            
        }
        async public void FindWeather(string url)
        {
            
            try
            {
                await data.TakeDay(url);
                presenter.TakeCity();
                
            }
            catch (HttpRequestException)
            {
                presenter.InvalidName();
            }
        }

        
    }
}