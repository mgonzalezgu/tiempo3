﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tiempo.Presenters;

namespace Tiempo.Models
{
    public class Interactor4 : IInteractor4
    {
        private Presenter4 presenter;
        TakeData data;

        public Interactor4(Presenter4 presenter4)
        {
            this.presenter = presenter4;
            data = new TakeData();
        }

        async public void TakeWeather(string url1)
        {
            await data.TakeHours(url1);
            var hour = data.hoursweather;
            presenter.TakeWeather(hour);
        }

        
    }
}