﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using Tiempo.Presenters;
using System.Threading.Tasks;

namespace Tiempo.Models
{
    public class TakeData
    {
        public DayWeather dayweather;
        public HoursWeather hoursweather;

        public TakeData()
        {
            dayweather = new DayWeather();
            hoursweather = new HoursWeather();
        }

        async public Task<DayWeather> TakeDay(string url)
        {
            var handler = new HttpClientHandler();
            HttpClient client = new HttpClient(handler);
            string result = await client.GetStringAsync(url);
            dayweather = JsonConvert.DeserializeObject<DayWeather>(result);
            return dayweather;
        }

        async public Task<HoursWeather> TakeHours(string url)
        {
            var handler = new HttpClientHandler();
            HttpClient client = new HttpClient(handler);
            string result = await client.GetStringAsync(url);
            hoursweather = JsonConvert.DeserializeObject<HoursWeather>(result);
            return hoursweather;
        }

        
    }
}