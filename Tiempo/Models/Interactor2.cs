﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiempo.Presenters;

namespace Tiempo.Models
{
    public class Interactor2 : IInteractor2
    {
        private IPresenter2 presenter;
        TakeData data;
        public Interactor2(Presenter2 presenter)
        {
            this.presenter = presenter;
            data = new TakeData();
        }
        async public void TakeWeather(string url1, string url2)
        {
            await data.TakeDay(url1);
            var day = data.dayweather;
            await data.TakeHours(url2);
            var hour = data.hoursweather;
            presenter.TakeWeather(day, hour);
        }
    }
}