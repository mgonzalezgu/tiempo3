﻿using System.Collections.Generic;
using TiempoCore.Models;
/*
namespace TiempoCore.Presenters
{
    public class TiempoPresentersImpl : TiempoPresenters1
    {
        
        private TiempoView1 view;
        private TiempoInteractorImpl1 interactor;
        public TiempoPresentersImpl(TiempoView1 view)
        {
            
            this.view = view;
            this.interactor = new TiempoInteractorImpl1(this);
        }
        
        public void DayWeather(string description, string temperature, string minmax, string icon,string country, string hum, string feels, string pres, string cloud, string speed1, string wind2, int sunr, int suns,List<Hour> weatherhours)
        {
            view.ShowOneDay(description, temperature, minmax, icon,country, hum, feels, pres, cloud,  speed1,  wind2,  sunr,  suns, weatherhours);
        }

        public void FindCity(string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                view.InvalidName();
            }
            else
            {
                interactor.FindWeather(city);

            }
        }

        
/*

void TiempoPresenters1.FindCity(string city,int num)
{
   if (string.IsNullOrEmpty(city))
   {
       view.InvalidName();
   }
   else
   {
       interactor.FindWeather(city,num);

   }
}



    void TiempoPresenters1.InvalidName()
        {
            view.InvalidName();
        }
        

/*
void Weather(DayWeather dayweather, HoursWeather hoursweather)
{
   view.ShowWeather(dayweather, hoursweather);
}

void TiempoPresenters1.Weather(DayWeather dayweather, HoursWeather hoursweather)
{
   throw new NotImplementedException();
}

    
}
*/