﻿/*
using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Android.Content;
using System.Collections.Generic;
using System.Net;
using Android.Graphics.Drawables;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;
using TiempoCore.Presenters;

namespace TiempoCore.Models
{
    public class TiempoInteractorImpl1:TiempoInteractor1
    {
        private TiempoPresenters1 presenter;
        //private DayWeather dayweather;
        private HoursWeather hoursweather;
        string url1;
        string url2;
        string apikey;
        string apiBase1;
        string apiBase2;
        string unit;
        public TiempoInteractorImpl1(TiempoPresenters1 presenter)
        {
            this.presenter = presenter;
            apikey = "828971f619b9ba9b1632c23dff01bec5";
            apiBase1 = "http://api.openweathermap.org/data/2.5/weather?q=";
            apiBase2 = "http://api.openweathermap.org/data/2.5/forecast?q=";
            unit = "metric";
            
        }
        async public void FindWeather(string city)
        {
            url1 = apiBase1 + city + "&appid=" + apikey + "&units=" + unit;
            try
            { 
                var handler = new HttpClientHandler();
                HttpClient client = new HttpClient(handler);
                string result = await client.GetStringAsync(url1);
                DayWeather dw = JsonConvert.DeserializeObject<DayWeather>(result);
                
                url2 = apiBase2 + city + "&appid=" + apikey + "&units=" + unit;
                var handler2 = new HttpClientHandler();
                HttpClient client2 = new HttpClient(handler2);
                string result2 = await client2.GetStringAsync(url2);
                hoursweather = JsonConvert.DeserializeObject<HoursWeather>(result2);
                
                string minmax = dw.main.temp_min.ToString() + "/" + dw.main.temp_max.ToString();
                var deg = dw.wind.deg;
                string direction;
                if (deg < 22.5 && deg > 337.5)
                {
                    direction = "North, " + deg.ToString() + "º";
                }
                else if (deg >= 22.5 && deg <= 67.5)
                {
                    direction = "Northeast, " + deg.ToString() + "º";
                }
                else if (deg > 67.5 && deg < 112.5)
                {
                    direction = "East, " + deg.ToString() + "º";
                }
                else if (deg >= 112.5 && deg <= 157.5)
                {
                    direction = "Southeast, " + deg.ToString() + "º";
                }
                else if (deg > 157.5 && deg < 202.5)
                {
                    direction = "South, " + deg.ToString() + "º";
                }
                else if (deg >= 202.5 && deg <= 247.5)
                {
                    direction = "Southwest, " + deg.ToString() + "º";
                }
                else if (deg > 247.5 && deg < 292.5)
                {
                    direction = "West, " + deg.ToString() + "º";
                }
                else
                {
                    direction = "Northwest, " + deg.ToString() + "º";
                }
                presenter.DayWeather(dw.weather[0].description,dw.main.temp.ToString(),minmax, dw.weather[0].icon,dw.sys.country,dw.main.humidity.ToString(),dw.main.feels_like.ToString(),dw.main.pressure.ToString(),dw.clouds.all.ToString(),dw.wind.speed.ToString(),direction,dw.sys.sunrise + dw.timezone, dw.sys.sunset+ dw.timezone);
                
            }
            catch (HttpRequestException)
            {
                presenter.InvalidName();
            }
        }

        
    }
}*/
